import React from 'react';
import persist from 'assets/js/persist';

const { Provider, Consumer } = React.createContext();

class AppContext extends React.Component {

  constructor(props) {
    super(props);
    this.setStatistics = this.setStatistics.bind(this);
    this.state = {
      statistics: persist.getLocal('statistics'),
      setStatistics: this.setStatistics
    };
  }

  setStatistics(statistics) {
    this.setState({ statistics }, () => { persist.setLocal('statistics', this.state.statistics); });
  }

  render() {
    return <Provider value={this.state}>{this.props.children}</Provider>
  }

}

export { AppContext as AppContextProvider, Consumer as AppContextConsumer };
