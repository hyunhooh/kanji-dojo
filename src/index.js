import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { createGlobalStyle } from 'styled-components';
import 'assets/css/tailwind.css';
import { AppContextProvider, AppContextConsumer } from 'contexts/AppContext';

const Welcome = React.lazy(() => import('pages/Welcome'));
const Learning = React.lazy(() => import('pages/Learning'));
const Result = React.lazy(() => import('pages/Result'));

const GlobalStyle = createGlobalStyle``;

ReactDOM.render(<>
  <GlobalStyle />
  <BrowserRouter>
    <AppContextProvider>
      <AppContextConsumer>
        {state => (
          <Switch>
            <Route exact path="/">
              <React.Suspense fallback={null}>
                <Welcome {...state} />
              </React.Suspense>
            </Route>
            <Route exact path="/Learning">
              <React.Suspense fallback={null}>
                <Learning {...state} />
              </React.Suspense>
            </Route>
            <Route exact path="/Result">
              <React.Suspense fallback={null}>
                <Result {...state} />
              </React.Suspense>
            </Route>
            <Route path="*">
              <React.Suspense fallback={null}>
                <div>PAGE_NOT_FOUND</div>
              </React.Suspense>
            </Route>
          </Switch>
        )}
      </AppContextConsumer>
    </AppContextProvider>
  </BrowserRouter></>,
  document.getElementById('root')
);
