import React from 'react';
import axios from 'axios';
import apiDescriptor from 'assets/js/api_descriptor';

let baseURL = '';

switch (process.env.REACT_APP_API_ENV) {
  case 'local': baseURL = 'http://localhost:3000'; break;
  case 'development': baseURL = ''; break;
  case 'production': baseURL = ''; break;
  default: baseURL = 'http://localhost:3000';
}

const instances = {
  test: axios.create({ baseURL: 'https://jsonplaceholder.typicode.com' }),
  default: axios.create({ baseURL })
};

Object.keys(instances).forEach(key => {
  instances[key].interceptors.request.use(config => {
    return config;
  }, err => {
    return Promise.reject(err);
  });
  instances[key].interceptors.response.use(response => {
    return response;
  }, err => {
    return Promise.reject(err);
  });
});

const api = async ({ descriptorName, headers, params, body }) => {
  try {
    const matchingApi = apiDescriptor[Object.keys(apiDescriptor).find(apiName => apiName === descriptorName)];
    return await instances[matchingApi.instanceName]({
      url: matchingApi.url,
      method: matchingApi.method,
      headers, params, body
    });
  } catch (err) {
    return Promise.reject(err);
  }
};

const withApi = WrappedComponent => class extends React.Component {

  render() {
    return <WrappedComponent {...this.props} api={api} />;
  }

};

export default withApi;
