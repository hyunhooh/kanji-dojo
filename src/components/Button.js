import React from 'react';
import styled from 'styled-components';
import Text from 'components/base_components/Text';

const DEFAULTS = {
  BG_COLOR: 'border-gray-800 bg-gray-800'
};

const backgroundColor = (props, className) => {
  if (props.danger) className = className.concat(` border-red-700 bg-red-700`).trim();
  if (props.primary) className = className.concat(` border-blue-700 bg-blue-700`).trim();
  else className = className.concat(` ${DEFAULTS.BG_COLOR}`).trim();
  return className.trim();
};

const StyledComponent = styled.button.attrs(props => {
  let className = 'border border-gray-800 rounded bg-gray-800 px-5 py-1 shadow';
  className = backgroundColor(props, className).trim();
  if (props.stretch) className = className.concat(' w-full').trim();
  if (props.dis) className = className.concat(' opacity-25 cursor-not-allowed').trim();
  return { className: className.trim() };
})``;

class Button extends React.Component {

  render () {
    return (
      <StyledComponent {...this.props}>
        <Text white>{this.props.children}</Text>
      </StyledComponent>
    );
  }

}

export default Button;
