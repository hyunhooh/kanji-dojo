import React from 'react';
import styled from 'styled-components';

const DEFAULTS = {
  FONT_WEIGHT: 'font-normal',
  TEXT_COLOR: 'text-gray-800'
};

const setFontWeight = (props, className) => {
  if (props.light) className = className.concat(` font-light`).trim();
  else className = className.concat(` ${DEFAULTS.FONT_WEIGHT}`).trim();
  return className.trim();
};

const textColor = (props, className) => {
  if (props.white) className = className.concat(` text-white`).trim();
  else className = className.concat(` ${DEFAULTS.TEXT_COLOR}`).trim();
  return className.trim();
};

const H1 = styled.h1.attrs(props => {
  let className = 'font-sans text-4xl md:text-5xl lg:text-6xl tracking-wide leading-relaxed break-normal whitespace-normal antialiased';
  className = setFontWeight(props, className).trim();
  className = textColor(props, className).trim();
  if (props.center) className = className.concat(' text-center').trim();
  return { className: className.trim() };
})``;
const H2 = styled.h2.attrs(props => {
  let className = 'font-sans text-3xl md:text-4xl lg:text-5xl tracking-wide leading-relaxed break-normal whitespace-normal antialiased';
  className = setFontWeight(props, className).trim();
  className = textColor(props, className).trim();
  if (props.center) className = className.concat(' text-center').trim();
  return { className: className.trim() };
})``;
const H3 = styled.h3.attrs(props => {
  let className = 'font-sans text-2xl md:text-3xl lg:text-4xl tracking-wide leading-relaxed break-normal whitespace-normal antialiased';
  className = setFontWeight(props, className).trim();
  className = textColor(props, className).trim();
  if (props.center) className = className.concat(' text-center').trim();
  return { className: className.trim() };
})``;
const H4 = styled.h4.attrs(props => {
  let className = 'font-sans text-xl md:text-2xl lg:text-3xl tracking-wide leading-relaxed break-normal whitespace-normal antialiased';
  className = setFontWeight(props, className).trim();
  className = textColor(props, className).trim();
  if (props.center) className = className.concat(' text-center').trim();
  return { className: className.trim() };
})``;
const H5 = styled.h5.attrs(props => {
  let className = 'font-sans text-lg md:text-xl lg:text-2xl tracking-wide leading-relaxed break-normal whitespace-normal antialiased';
  className = setFontWeight(props, className).trim();
  className = textColor(props, className).trim();
  if (props.center) className = className.concat(' text-center').trim();
  return { className: className.trim() };
})``;
const P = styled.p.attrs(props => {
  let className = 'font-sans text-base tracking-wide leading-relaxed break-normal whitespace-normal antialiased';
  className = setFontWeight(props, className).trim();
  className = textColor(props, className).trim();
  if (props.center) className = className.concat(' text-center').trim();
  return { className: className.trim() };
})``;

class Text extends React.Component {

  constructor (props) {
    super(props);
    this.selectTag = this.selectTag.bind(this);
  }

  selectTag () {
    let selectedComponent = null;
    if (this.props.h1) selectedComponent = H1;
    else if (this.props.h2) selectedComponent = H2;
    else if (this.props.h3) selectedComponent = H3;
    else if (this.props.h4) selectedComponent = H4;
    else if (this.props.h5) selectedComponent = H5;
    else selectedComponent = P;
    return selectedComponent;
  }

  render () {
    const SelectedComponent = this.selectTag();
    return <SelectedComponent {...this.props}>{this.props.children}</SelectedComponent>;
  }

}

export default Text;
