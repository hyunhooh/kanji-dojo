import React from 'react';
import { withRouter } from 'react-router-dom';
import Button from 'components/Button';
import Text from 'components/base_components/Text';

class Result extends React.Component {

  constructor (props) {
    super(props);
    this.handleTryAgain = this.handleTryAgain.bind(this);
  }

  handleTryAgain () {
    this.props.history.push('/');
  }
  
  render () {
    const statistics = this.props.statistics;
    const numCorrect = statistics.reduce((acc, cur) => {
      if (cur.correct) acc += 1;
      return acc;
    }, 0);
    const avgTimeSpend = statistics.reduce((acc, cur) => {
      return acc += cur.timeConsumed;
    }, 0) / statistics.length;
    return (<>
      <div className="flex justify-center mt-10 md:mt-20">
        <Text h2 light>Result</Text>
      </div>
      <div className="flex justify-center px-2">
        <div className="w-128 shadow divide-y mt-10 p-8">
          <div className="py-3 text-6xl text-center font-score">{numCorrect} / {statistics.length}</div>
          <div className="py-10 text-center">
            <Text h4>{avgTimeSpend} sec / kanji</Text>
          </div>
        </div>
      </div>
      <div className="text-center mt-5">
        <Button onClick={this.handleTryAgain}>Try again</Button>
      </div>
    </>);
  }

}

export default withRouter(Result);
