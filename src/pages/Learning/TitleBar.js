import React from 'react';
import styled from 'styled-components';
import Text from 'components/base_components/Text';

const Wrapper = styled.div.attrs(props => {
  let className = 'px-3 flex justify-between items-center border-b border-gray-800';
  return { className: className.trim() };
})``;

class TitleBar extends React.Component {

  render () {
    return (
      <Wrapper>
        <Text h4 light>{this.props.children}</Text>
        <Text>{this.props.currentQuestion} out of {this.props.totalQuestions}</Text>
      </Wrapper>
    );
  }

}

export default TitleBar;
