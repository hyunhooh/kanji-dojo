import React from 'react';
import styled from 'styled-components';
import { TIMER, NEXT_TIMER } from 'pages/Learning'
import Text from 'components/base_components/Text';
import Button from 'components/Button';
import Sound from 'icons/Sound';

const Wrapper = styled.div.attrs(props => {
  let className = 'flex flex-wrap border-b border-gray-800';
  return { className: className.trim() };
})``;

const LeftSection = styled.div.attrs(props => {
  let className = 'w-full md:w-2/3 md:border-r md:border-gray-800';
  return { className: className.trim() };
})``;

const Kanji = styled.div.attrs(props => {
  let className = 'border-3 rounded mr-1';
  if (props.isWrong) className = className.concat(' border-red-500').trim();
  else className = className.concat(' border-blue-500').trim();
  return { className: className.trim() };
})``;

const KanjiWrapper = styled.div.attrs(props => {
  let className = 'flex justify-center items-start py-10';
  return { className: className.trim() };
})``;

const RightSection = styled.div.attrs(props => {
  let className = 'w-full md:w-1/3 p-3 flex flex-col justify-between';
  return { className: className.trim() };
})``;

class MainSection extends React.Component {

  render () {
    return (
      <Wrapper>
        <LeftSection>
          <KanjiWrapper>
            <Kanji {...this.props}>
              <Text h1>{this.props.children}</Text>
            </Kanji>
            <Sound />
          </KanjiWrapper>
        </LeftSection>
        <RightSection>
          <div className="h-5 bg-gray-500 rounded mb-1">
            {this.props.isWrong ? (
              <div style={{
                width: `${(1 - (this.props.nextTimer / NEXT_TIMER)) * 100}%`
              }} className="w-0 h-full bg-red-700 rounded"></div>
            ) : (
              <div style={{
                width: `${!this.props.showResult ? (1 - (this.props.timer / TIMER)) * 100 : 100}%`
              }} className="w-0 h-full bg-green-700 rounded"></div>
            )}
          </div>
          <div>
            <Button danger stretch dis={this.props.showResult} onClick={() => {
              if (!this.props.showResult) this.props.handleSkipClick();
            }}>SKIP</Button>
            <div className="mb-1"></div>
            <Button primary stretch dis={!this.props.showResult} onClick={() => {
              if (this.props.showResult) this.props.handleResultClick();
            }}>RESULT</Button>
          </div>
        </RightSection>
      </Wrapper>
    );
  }

}

export default MainSection;
