import React from 'react';
import styled from 'styled-components';
import { v4 } from 'uuid';
import Text from 'components/base_components/Text';

const AnswerHiragana = styled.div.attrs(props => {
  // "w-full md:w-1/3 py-3 border-b border-gray-500 md:py-10 md:border-none cursor-pointer hover:bg-gray-400"
  let className = 'w-full md:w-1/3 py-3 md:py-10 cursor-pointer';
  if (props.answerId === props.questionId && props.isWrong) className = className.concat(' border-3 border-green-500').trim();
  else className = className.concat(' border-b border-gray-500 md:border-none').trim();
  return { className: className.trim() }
})``;

class BottomSection extends React.Component {

  render () {
    return (<>
      <div className="flex flex-wrap">
        {this.props.answerPool.map(answer => (
          <AnswerHiragana
            key={v4()}
            isWrong={this.props.isWrong}
            answerId={answer.id}
            questionId={this.props.selectedCombination.id}
            onClick={() => this.props.handleAnswerClick(answer)}>
            <Text h2 center>{answer.hiragana}</Text>
          </AnswerHiragana>
        ))}
      </div>
    </>);
  }

}

export default BottomSection;
