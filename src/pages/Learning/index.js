import React from 'react';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import TitleBar from 'pages/Learning/TitleBar';
import MainSection from 'pages/Learning/MainSection';
import BottomSection from 'pages/Learning/BottomSection';
import dictionary from 'assets/data/dictionary.json';
import persist from 'assets/js/persist';

const MAIN_TITLE = 'LEARNING KANJI';
const TOTAL_QUESTIONS = 10;
export const TIMER = 25;
export const NEXT_TIMER = 5;

const FullScreen = styled.div.attrs(props => {
  let className = 'absolute top-0 left-0 w-full h-full';
  return { className: className.trim() }
})``;

const OuterBox = styled.div.attrs(props => {
  let className = 'flex w-full h-full justify-center items-center px-3';
  return { className: className.trim() }
})``;

const InnerBox = styled.div.attrs(props => {
  let className = 'w-md rounded border-4 border-double border-gray-800 shadow-xl bg-gray-200';
  return { className: className.trim() }
})``;

const getRandomInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

// Fisher–Yates shuffle algorithm
// https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#The_modern_algorithm
// from lowest index to highest
const shuffle = arr => {
  let temp = null;
  for (let i = 0; i < arr.length - 2; i += 1) {
    const j = getRandomInt(i, arr.length);
    temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
  }
  return arr;
};

class Learning extends React.Component {

  constructor (props) {
    super(props);
    this.updateCombinations = this.updateCombinations.bind(this);
    this.updateAnswerPool = this.updateAnswerPool.bind(this);
    this.handleAnswerClick = this.handleAnswerClick.bind(this);
    this.setTimerInterval = this.setTimerInterval.bind(this);
    this.clearTimerInterval = this.clearTimerInterval.bind(this);
    this.whenWrong = this.whenWrong.bind(this);
    this.whenCorrect = this.whenCorrect.bind(this);
    this.whenEnter = this.whenEnter.bind(this);
    this.handleSkipClick = this.handleSkipClick.bind(this);
    this.handleSeeResult = this.handleSeeResult.bind(this);
    this.careSelection = this.careSelection.bind(this);
    this.state = {
      combinations: persist.getLocal('combinations') || dictionary.combinations,
      selectedCombination: [],
      answerPool: [],
      currentQuestionNumber: 0,
      isLast: true,
      timer: TIMER,
      nextTimer: NEXT_TIMER,
      isWrong: false,
      showResult: false
    };
    this.intervals = {
      timer: null,
      nextTimer: null
    };
    this.statistics = [];
    this.initialCombinations = this.state.combinations;
  }

  componentDidMount () {
    if (!persist.getLocal('combinations')) {
      persist.setLocal('combinations', this.state.combinations);
    }
    this.updateCombinations();
    this.setTimerInterval('timer', () => {
      this.handleAnswerClick(null);
    });
  }

  componentWillUnmount () {
    this.clearTimerInterval('timer');
    this.clearTimerInterval('nextTimer');
  }

  setTimerInterval (type, callback) {
    this.intervals[type] = window.setInterval(() => {
      this.setState(state => ({ [type]: state[type] - 1 }), () => {
        if (this.state[type] < 1 && this.intervals[type]) {
          this.clearTimerInterval(type);
          if (callback) callback();
        }
      });
    }, 1000);
  }

  clearTimerInterval (type) {
    if (this.intervals[type]) window.clearInterval(this.intervals[type]);
  }

  careSelection (weightedCombination) {
    // find lowest weight number
    let lowestNumber = -1;
    weightedCombination.forEach(combination => {
      if (!combination.weight) combination.weight = 0;
      if (lowestNumber === -1) lowestNumber = combination.weight;
      if (combination.weight < lowestNumber) lowestNumber = combination.weight;
    });
    // filter combinations with the number
    const filteredCombination = weightedCombination.filter(combination => combination.weight === lowestNumber);
    // get random int using the list
    const randomInt = getRandomInt(0, filteredCombination.length);
    // select one combination using the random int
    const selectedCombination = filteredCombination[randomInt];
    // return the combination
    return selectedCombination;
  }

  updateCombinations () {
    window.removeEventListener('keyup', this.whenEnter, true);
    this.setState({ isWrong: false });
    this.setState(state => {
      const currentQuestionNumber = state.currentQuestionNumber < TOTAL_QUESTIONS
        ? state.currentQuestionNumber + 1
        : state.currentQuestionNumber;
      return { currentQuestionNumber };
    });
    const selectedCombination = this.careSelection(this.state.combinations);
    const combinations = this.state.combinations.filter(combination => combination.id !== selectedCombination.id);
    const answerPool = [];
    answerPool.push(selectedCombination);
    const { firstRandomCombination, secondRandomCombination } = this.updateAnswerPool(selectedCombination);
    answerPool.push(firstRandomCombination);
    answerPool.push(secondRandomCombination);
    shuffle(answerPool);
    this.initialCombinations = this.initialCombinations.map(combination => {
      if (combination.id === selectedCombination.id) {
        if (!combination.weight) {
          combination.weight = 0;
        }
        combination.weight += 1;
      }
      return combination;
    });
    persist.setLocal('combinations', this.initialCombinations);
    this.setState({
      combinations,
      selectedCombination,
      answerPool
    });
  }

  updateAnswerPool (selectedCombination) {
    const clonedCombination = selectedCombination;
    let firstRandomCombination = this.state.combinations[getRandomInt(0, this.state.combinations.length)];
    let secondRandomCombination = this.state.combinations[getRandomInt(0, this.state.combinations.length)];
    if (clonedCombination.id === firstRandomCombination.id
      || clonedCombination.id === secondRandomCombination.id
      || firstRandomCombination.id === secondRandomCombination.id) {
        const updatedPool = this.updateAnswerPool(clonedCombination);
        firstRandomCombination = updatedPool.firstRandomCombination;
        secondRandomCombination = updatedPool.secondRandomCombination;
      };
    return { firstRandomCombination, secondRandomCombination };
  }

  handleAnswerClick (answer) {
    if (this.state.currentQuestionNumber <= TOTAL_QUESTIONS && !this.state.isWrong) {
      if (this.state.currentQuestionNumber < TOTAL_QUESTIONS) {
        if (answer && answer.id === this.state.selectedCombination.id) {
          this.whenCorrect(this.state.selectedCombination, answer, true);
        }
        else {
          this.whenWrong(this.state.selectedCombination, answer, true);
        }
      }
      else {
        if (this.state.isLast) {
          this.setState({ isLast: false });
          if (answer && answer.id === this.state.selectedCombination.id) {
            this.whenCorrect(this.state.selectedCombination, answer, false);
          }
          else {
            this.whenWrong(this.state.selectedCombination, answer, false);
          }
          this.setState({ showResult: true });
        }
      }
    }
  }

  whenCorrect(question, userAnswer, isUpdate) {
    this.statistics.push({
      correct: true,
      timeConsumed: TIMER - this.state.timer,
      question,
      userAnswer
    });
    if (isUpdate) {
      this.updateCombinations();
      this.clearTimerInterval('timer');
      this.setState({ timer: TIMER });
      this.setTimerInterval('timer', () => {
        this.handleAnswerClick(null);
      });
    }
  }

  whenWrong(question, userAnswer, isUpdate) {
    this.statistics.push({
      correct: false,
      timeConsumed: TIMER - this.state.timer,
      question,
      userAnswer
    });
    this.setState({
      isWrong: true
    });
    if (isUpdate) {
      this.clearTimerInterval('timer');
      this.clearTimerInterval('nextTimer');
      this.setState({ nextTimer: NEXT_TIMER });
      this.setTimerInterval('nextTimer', () => {
        this.updateCombinations();
        this.clearTimerInterval('timer');
        this.setState({ timer: TIMER });
        this.setTimerInterval('timer', () => {
          this.handleAnswerClick(null);
        });
      });
      window.addEventListener('keyup', this.whenEnter, true);
    }
  }

  whenEnter () {
    this.clearTimerInterval('nextTimer');
    this.updateCombinations();
    this.clearTimerInterval('timer');
    this.setState({ timer: TIMER });
    this.setTimerInterval('timer', () => {
      this.handleAnswerClick(null);
    });
  }

  handleSkipClick () {
    this.handleAnswerClick(null);
    if (this.state.isWrong) {
      this.whenEnter();
    }
  }

  handleSeeResult () {
    this.props.setStatistics(this.statistics);
    this.props.history.push('/Result');
  }

  render () {
    return (
      <FullScreen>
        <OuterBox>
          <InnerBox>
            <TitleBar currentQuestion={this.state.currentQuestionNumber} totalQuestions={TOTAL_QUESTIONS}>{MAIN_TITLE}</TitleBar>
            <MainSection
              isWrong={this.state.isWrong}
              showResult={this.state.showResult}
              nextTimer={this.state.nextTimer}
              timer={this.state.timer}
              handleSkipClick={this.handleSkipClick}
              handleResultClick={this.handleSeeResult}>
              {this.state.selectedCombination.kanji || ''}
            </MainSection>
            <BottomSection
              answerPool={this.state.answerPool}
              isWrong={this.state.isWrong}
              selectedCombination={this.state.selectedCombination}
              handleAnswerClick={this.handleAnswerClick}/>
          </InnerBox>
        </OuterBox>
      </FullScreen>
    );
  }

}

export default withRouter(Learning);