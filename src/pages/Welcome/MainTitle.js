import React from 'react';
import Text from 'components/base_components/Text';

class MainTitle extends React.Component {

  render () {
    return <Text h1 light>{this.props.children}</Text>;
  }

}

export default MainTitle;
