import React from 'react';
import styled from 'styled-components';
import TopRightLogo from 'pages/Welcome/TopRightLogo';
import MainTitle from 'pages/Welcome/MainTitle';
import MainDescription from 'pages/Welcome/MainDescription';
import StartButton from 'pages/Welcome/StartButton';
import KanjiLogo from 'assets/img/kanji.png';

const MAIN_TITLE = 'KANJI DOJO';
const MAIN_DESCRIPTION = 'Kanji Dojo is an application that helps people learning Japanese Kanji characters using Hiragana. Before using this application, you need to have basic understanding in Hiragana. This app was built for mobile first.';
const START_BUTTON = 'Start Learning';

const Wrapper = styled.div.attrs(props => {
  let className = 'absolute top-0 w-full h-full flex flex-col justify-center items-center';
  return { className: className.trim() };
})``;

class Welcome extends React.Component {
  
  render () {
    return (<>
      <TopRightLogo src={KanjiLogo} />
      <Wrapper>
        <MainTitle>{MAIN_TITLE}</MainTitle>
        <MainDescription>{MAIN_DESCRIPTION}</MainDescription>
        <StartButton>{START_BUTTON}</StartButton>
      </Wrapper>
    </>);
  }

}

export default Welcome;
