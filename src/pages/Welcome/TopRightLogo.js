import React from 'react';
import styled from 'styled-components';

const LogoWrapper = styled.div.attrs(props => {
  let className = 'absolute top-3 right-3 md:top-5 md:right-5 w-10 h-10 md:w-16 md:h-16';
  return { className: className.trim() };
})`
  @keyframes spin {
    100% { transform: rotate(360deg); }
  }
  animation:spin 4s linear infinite;
`;

const Logo = styled.img.attrs(props => {
  let className = 'w-full h-full';
  return {
    className: className.trim(),
    src: props.src,
    alt: 'Logo'
  };
})``;

class TopRightLogo extends React.Component {

  render () {
    return (
      <LogoWrapper>
        <Logo {...this.props} />
      </LogoWrapper>
    );
  }

}

export default TopRightLogo;
