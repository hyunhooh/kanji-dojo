import React from 'react';
import styled from 'styled-components';
import Text from 'components/base_components/Text';

const Wrapper = styled.div.attrs(props => {
  let className = 'w-80 md:w-148 text-center';
  return { className: className.trim() };
})``;

class MainDescription extends React.Component {

  render () {
    return (
      <Wrapper>
        <Text>{this.props.children}</Text>
      </Wrapper>
    );
  }

}

export default MainDescription;
