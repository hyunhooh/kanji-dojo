import React from 'react';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import Button from 'components/Button';

const TopMargin = styled.div.attrs(props => {
  let className = 'mt-10';
  return { className: className.trim() };
})``;

class StartButton extends React.Component {

  constructor (props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick () {
    this.props.history.push('/Learning');
  }

  render () {
    return (<>
      <TopMargin />
      <Button
        {...this.props}
        onClick={this.handleClick}>
        {this.props.children}
      </Button>
    </>);
  }

}

export default withRouter(StartButton);
