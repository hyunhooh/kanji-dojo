const setLocal = (key, value) => {
  if (value) window.localStorage.setItem(key, JSON.stringify(value));
};

const getLocal = key => {
  const savedItem = window.localStorage.getItem(key);
  if (savedItem && savedItem !== 'undefined') {
    return JSON.parse(window.localStorage.getItem(key));
  }
  else {
    return null;
  }
};

const removeLocal = key => {
  window.localStorage.removeItem(key);
};

const clearLocal = () => {
  window.localStorage.clear();
};

const saveCookie = () => {
  
};

const loadCookie = () => {
  
};

export default {
  setLocal,
  getLocal,
  removeLocal,
  clearLocal,
  saveCookie,
  loadCookie
};
