export default {

  getTestUsers: {
    instanceName: 'test',
    url: '/users',
    method: 'GET',
    requiredHeaders: [],
    rquiredParams: [],
    requiredBody: []
  },

  getTestPosts: {
    instanceName: 'test',
    url: '/posts',
    method: 'GET',
    requiredHeaders: [],
    rquiredParams: [],
    requiredBody: []
  },

  createTestPost: {
    instanceName: 'test',
    url: '/posts',
    method: 'POST',
    requiredHeaders: [],
    rquiredParams: [],
    requiredBody: {
      title: 'foo',
      body: 'bar',
      userId: 1
    }
  }

};
