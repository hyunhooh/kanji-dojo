# KANJI DOJO

Kanjo Dojo is an web based application that helps people learning Japanese Kanji charactoers using Hiragana.

>**Click [here](https://ohhobs.com) to see the live application now!**

## System Requirements

* Node.js (12.16.1 or higher)
* NPM (6.14.4 or higher)

## Tools & Frameworks

* HTML/CSS/JS
* React, React Router, Styled Components
* Axios
* PostCSS/TailwindCSS/PurgeCSS

## Hosting Environment

* AWS EC2 / Load Balancer
* Node.js Express server
* Ubuntu

## Installation

* Clone or download this repository
* In your terminal, type `npm install`
* After finish install packages, type `npm start`
* Reload your browser one more time after you see white screen on your browser

## Functionality Check List

* Loading data from `src/assets/data/dictionary.json`
* Picking 10 Kanji characters based on their weight which is making sure that they are not repeating too often
* Picking 3 hiragana answers - 1 correct and 2 randoms
* Using Fisher–Yates algorithm to shuffle answers
* Timer for each question and delay before the next question
* Answer indicator when users pick wrong answer or no answer in 25 seconds
* Skip to the next question when users press any key
* Mobile first UI
* Basic API setup for future implementation
* In result page, showing required status - total number of correct answer and an average time user spend on answering questions
